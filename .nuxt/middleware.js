const middleware = {}

middleware['auth'] = require('../resources/middleware/auth.js')
middleware['auth'] = middleware['auth'].default || middleware['auth']

middleware['loggedIn'] = require('../resources/middleware/loggedIn.js')
middleware['loggedIn'] = middleware['loggedIn'].default || middleware['loggedIn']

export default middleware
