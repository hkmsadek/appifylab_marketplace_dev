import Vue from 'vue'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from '../resources/layouts/error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'
import { createStore } from './store.js'

/* Plugins */

import nuxt_plugin_adsbygoogle_0e2bd398 from 'nuxt_plugin_adsbygoogle_0e2bd398' // Source: ./adsbygoogle.js (mode: 'all')
import nuxt_plugin_moment_5231539a from 'nuxt_plugin_moment_5231539a' // Source: ./moment.js (mode: 'all')
import nuxt_plugin_axios_a0ce1484 from 'nuxt_plugin_axios_a0ce1484' // Source: ./axios.js (mode: 'all')
import nuxt_plugin_googleanalytics_2e4bffa8 from 'nuxt_plugin_googleanalytics_2e4bffa8' // Source: ./google-analytics.js (mode: 'client')
import nuxt_plugin_axios_fb9c9a02 from 'nuxt_plugin_axios_fb9c9a02' // Source: ../resources/plugins/axios (mode: 'all')
import nuxt_plugin_iview_fabcf5d2 from 'nuxt_plugin_iview_fabcf5d2' // Source: ../resources/plugins/iview (mode: 'all')
import nuxt_plugin_vuesax_30a3318a from 'nuxt_plugin_vuesax_30a3318a' // Source: ../resources/plugins/vuesax (mode: 'all')
import nuxt_plugin_commonModules_0762b4a5 from 'nuxt_plugin_commonModules_0762b4a5' // Source: ../resources/plugins/commonModules (mode: 'all')
import nuxt_plugin_vuelazyload_f1f534be from 'nuxt_plugin_vuelazyload_f1f534be' // Source: ../resources/plugins/vue-lazy-load (mode: 'all')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"page","mode":"out-in","appear":false,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

async function createApp (ssrContext) {
  const router = await createRouter(ssrContext)

  const store = createStore(ssrContext)
  // Add this.$router into store actions/mutations
  store.$router = router

  // Fix SSR caveat https://github.com/nuxt/nuxt.js/issues/3757#issuecomment-414689141
  const registerModule = store.registerModule
  store.registerModule = (path, rawModule, options) => registerModule.call(store, path, rawModule, Object.assign({ preserveState: process.client }, options))

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"title":"Appifylab MarketPlace","meta":[{"name":"robots","content":"noindex,noarchive,nofollow"},{"charset":"utf-8"},{"name":"viewport","content":"width=device-width, initial-scale=1"},{"property":"fb:pages","content":"310060563192757"},{"hid":"description","name":"description","content":"Appifylab MarketPlace"}],"link":[{"rel":"icon","type":"image\u002Fx-icon","href":"\u002Fstatic\u002FAppifylogo.png"},{"rel":"stylesheet","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss?family=Lato:400,400i,700,700i|Lora:400,400i,700,700i&display=swap"},{"rel":"stylesheet","href":"https:\u002F\u002Fuse.fontawesome.com\u002Freleases\u002Fv5.8.2\u002Fcss\u002Fall.css","integrity":"sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq\u002F1cq5FLXAZQ7Ay","crossorigin":"anonymous"},{"rel":"stylesheet","href":"\u002Fcss\u002Fbootstrap.css"},{"rel":"stylesheet","href":"\u002Fcss\u002Fcommon.css"},{"rel":"stylesheet","href":"\u002Fcss\u002Fmain.css"}],"script":[{"src":"https:\u002F\u002Fcode.jquery.com\u002Fjquery-3.4.0.min.js","integrity":"sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=","crossorigin":"anonymous"},{"src":"\u002Fjs\u002Fscript.js","body":true},{"src":"https:\u002F\u002Fwww.google.com\u002Frecaptcha\u002Fapi.js?onload=vueRecaptchaApiLoaded&render=explicit","async":true,"defer":true},{"src":"https:\u002F\u002Fjs.stripe.com\u002Fv3\u002F","body":true},{"hid":"adsbygoogle-script","async":true,"src":"\u002F\u002Fpagead2.googlesyndication.com\u002Fpagead\u002Fjs\u002Fadsbygoogle.js"},{"hid":"adsbygoogle","innerHTML":"if (!window.__abg_called){ (window.adsbygoogle = window.adsbygoogle || []).push({\n    google_ad_client: \"ca-google\",\n    enable_page_level_ads: false,\n    overlays: {bottom: false}\n  }); window.__abg_called = true;}"}],"style":[],"__dangerouslyDisableSanitizersByTagID":{"adsbygoogle":["innerHTML"]}},

    store,
    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  // Make app available into store via this.app
  store.app = app

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    store,
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  const inject = function (key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value

    // Add into store
    store[key] = app[key]

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  if (process.client) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }

  // Plugin execution

  if (typeof nuxt_plugin_adsbygoogle_0e2bd398 === 'function') {
    await nuxt_plugin_adsbygoogle_0e2bd398(app.context, inject)
  }

  if (typeof nuxt_plugin_moment_5231539a === 'function') {
    await nuxt_plugin_moment_5231539a(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_a0ce1484 === 'function') {
    await nuxt_plugin_axios_a0ce1484(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_googleanalytics_2e4bffa8 === 'function') {
    await nuxt_plugin_googleanalytics_2e4bffa8(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_fb9c9a02 === 'function') {
    await nuxt_plugin_axios_fb9c9a02(app.context, inject)
  }

  if (typeof nuxt_plugin_iview_fabcf5d2 === 'function') {
    await nuxt_plugin_iview_fabcf5d2(app.context, inject)
  }

  if (typeof nuxt_plugin_vuesax_30a3318a === 'function') {
    await nuxt_plugin_vuesax_30a3318a(app.context, inject)
  }

  if (typeof nuxt_plugin_commonModules_0762b4a5 === 'function') {
    await nuxt_plugin_commonModules_0762b4a5(app.context, inject)
  }

  if (typeof nuxt_plugin_vuelazyload_f1f534be === 'function') {
    await nuxt_plugin_vuelazyload_f1f534be(app.context, inject)
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, () => {
        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from, next) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    store,
    app,
    router
  }
}

export { createApp, NuxtError }
