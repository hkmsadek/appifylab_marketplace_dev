import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _7109c712 = () => interopDefault(import('../resources/pages/checkout.vue' /* webpackChunkName: "pages/checkout" */))
const _3fbe5469 = () => interopDefault(import('../resources/pages/contact_us.vue' /* webpackChunkName: "pages/contact_us" */))
const _4ae184b6 = () => interopDefault(import('../resources/pages/license.vue' /* webpackChunkName: "pages/license" */))
const _4ede37cd = () => interopDefault(import('../resources/pages/login.vue' /* webpackChunkName: "pages/login" */))
const _b616fe2c = () => interopDefault(import('../resources/pages/payment.vue' /* webpackChunkName: "pages/payment" */))
const _7ac7ff15 = () => interopDefault(import('../resources/pages/privacy_policy.vue' /* webpackChunkName: "pages/privacy_policy" */))
const _33bad060 = () => interopDefault(import('../resources/pages/profile/index.vue' /* webpackChunkName: "pages/profile/index" */))
const _017005a6 = () => interopDefault(import('../resources/pages/services/index.vue' /* webpackChunkName: "pages/services/index" */))
const _4d7aa238 = () => interopDefault(import('../resources/pages/signup.vue' /* webpackChunkName: "pages/signup" */))
const _2f3452eb = () => interopDefault(import('../resources/pages/terms.vue' /* webpackChunkName: "pages/terms" */))
const _da000418 = () => interopDefault(import('../resources/pages/ticketCreate.vue' /* webpackChunkName: "pages/ticketCreate" */))
const _32f5925e = () => interopDefault(import('../resources/pages/userReplay.vue' /* webpackChunkName: "pages/userReplay" */))
const _0f607b9f = () => interopDefault(import('../resources/pages/cart/details.vue' /* webpackChunkName: "pages/cart/details" */))
const _3cb43c06 = () => interopDefault(import('../resources/pages/cart/edit.vue' /* webpackChunkName: "pages/cart/edit" */))
const _7858b52f = () => interopDefault(import('../resources/pages/cart/select.vue' /* webpackChunkName: "pages/cart/select" */))
const _38e619f1 = () => interopDefault(import('../resources/pages/done/cart.vue' /* webpackChunkName: "pages/done/cart" */))
const _4cf0829b = () => interopDefault(import('../resources/pages/done/cartEdit.vue' /* webpackChunkName: "pages/done/cartEdit" */))
const _3b7da64d = () => interopDefault(import('../resources/pages/done/cartSelect.vue' /* webpackChunkName: "pages/done/cartSelect" */))
const _7a0b5d2e = () => interopDefault(import('../resources/pages/done/product.vue' /* webpackChunkName: "pages/done/product" */))
const _7b7abc64 = () => interopDefault(import('../resources/pages/done/productDetails.vue' /* webpackChunkName: "pages/done/productDetails" */))
const _600303f4 = () => interopDefault(import('../resources/pages/done/service.vue' /* webpackChunkName: "pages/done/service" */))
const _5a981cab = () => interopDefault(import('../resources/pages/done/serviceRequest.vue' /* webpackChunkName: "pages/done/serviceRequest" */))
const _4744c4ed = () => interopDefault(import('../resources/pages/not_done/page1.vue' /* webpackChunkName: "pages/not_done/page1" */))
const _4752dc6e = () => interopDefault(import('../resources/pages/not_done/page2.vue' /* webpackChunkName: "pages/not_done/page2" */))
const _4760f3ef = () => interopDefault(import('../resources/pages/not_done/page3.vue' /* webpackChunkName: "pages/not_done/page3" */))
const _a6f52ec6 = () => interopDefault(import('../resources/pages/not_done/price_calculator.vue' /* webpackChunkName: "pages/not_done/price_calculator" */))
const _44f2bffa = () => interopDefault(import('../resources/pages/profile/download.vue' /* webpackChunkName: "pages/profile/download" */))
const _12ee4a80 = () => interopDefault(import('../resources/pages/profile/myOrder.vue' /* webpackChunkName: "pages/profile/myOrder" */))
const _1a62dd5e = () => interopDefault(import('../resources/pages/profile/profileEdit.vue' /* webpackChunkName: "pages/profile/profileEdit" */))
const _22b16e15 = () => interopDefault(import('../resources/pages/profile/settings.vue' /* webpackChunkName: "pages/profile/settings" */))
const _74a0b02a = () => interopDefault(import('../resources/pages/services/request_for_service.vue' /* webpackChunkName: "pages/services/request_for_service" */))
const _2af04372 = () => interopDefault(import('../resources/pages/user/email-verify.vue' /* webpackChunkName: "pages/user/email-verify" */))
const _05e1a3c7 = () => interopDefault(import('../resources/pages/user/password-token-verify.vue' /* webpackChunkName: "pages/user/password-token-verify" */))
const _a238acfe = () => interopDefault(import('../resources/pages/user/reset-password.vue' /* webpackChunkName: "pages/user/reset-password" */))
const _7cd4be84 = () => interopDefault(import('../resources/pages/products/details/_name.vue' /* webpackChunkName: "pages/products/details/_name" */))
const _2f709ff7 = () => interopDefault(import('../resources/pages/profile/order/_id.vue' /* webpackChunkName: "pages/profile/order/_id" */))
const _fdcca56a = () => interopDefault(import('../resources/pages/products/_name.vue' /* webpackChunkName: "pages/products/_name" */))
const _f592d694 = () => interopDefault(import('../resources/pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/checkout",
    component: _7109c712,
    name: "checkout"
  }, {
    path: "/contact_us",
    component: _3fbe5469,
    name: "contact_us"
  }, {
    path: "/license",
    component: _4ae184b6,
    name: "license"
  }, {
    path: "/login",
    component: _4ede37cd,
    name: "login"
  }, {
    path: "/payment",
    component: _b616fe2c,
    name: "payment"
  }, {
    path: "/privacy_policy",
    component: _7ac7ff15,
    name: "privacy_policy"
  }, {
    path: "/profile",
    component: _33bad060,
    name: "profile"
  }, {
    path: "/services",
    component: _017005a6,
    name: "services"
  }, {
    path: "/signup",
    component: _4d7aa238,
    name: "signup"
  }, {
    path: "/terms",
    component: _2f3452eb,
    name: "terms"
  }, {
    path: "/ticketCreate",
    component: _da000418,
    name: "ticketCreate"
  }, {
    path: "/userReplay",
    component: _32f5925e,
    name: "userReplay"
  }, {
    path: "/cart/details",
    component: _0f607b9f,
    name: "cart-details"
  }, {
    path: "/cart/edit",
    component: _3cb43c06,
    name: "cart-edit"
  }, {
    path: "/cart/select",
    component: _7858b52f,
    name: "cart-select"
  }, {
    path: "/done/cart",
    component: _38e619f1,
    name: "done-cart"
  }, {
    path: "/done/cartEdit",
    component: _4cf0829b,
    name: "done-cartEdit"
  }, {
    path: "/done/cartSelect",
    component: _3b7da64d,
    name: "done-cartSelect"
  }, {
    path: "/done/product",
    component: _7a0b5d2e,
    name: "done-product"
  }, {
    path: "/done/productDetails",
    component: _7b7abc64,
    name: "done-productDetails"
  }, {
    path: "/done/service",
    component: _600303f4,
    name: "done-service"
  }, {
    path: "/done/serviceRequest",
    component: _5a981cab,
    name: "done-serviceRequest"
  }, {
    path: "/not_done/page1",
    component: _4744c4ed,
    name: "not_done-page1"
  }, {
    path: "/not_done/page2",
    component: _4752dc6e,
    name: "not_done-page2"
  }, {
    path: "/not_done/page3",
    component: _4760f3ef,
    name: "not_done-page3"
  }, {
    path: "/not_done/price_calculator",
    component: _a6f52ec6,
    name: "not_done-price_calculator"
  }, {
    path: "/profile/download",
    component: _44f2bffa,
    name: "profile-download"
  }, {
    path: "/profile/myOrder",
    component: _12ee4a80,
    name: "profile-myOrder"
  }, {
    path: "/profile/profileEdit",
    component: _1a62dd5e,
    name: "profile-profileEdit"
  }, {
    path: "/profile/settings",
    component: _22b16e15,
    name: "profile-settings"
  }, {
    path: "/services/request_for_service",
    component: _74a0b02a,
    name: "services-request_for_service"
  }, {
    path: "/user/email-verify",
    component: _2af04372,
    name: "user-email-verify"
  }, {
    path: "/user/password-token-verify",
    component: _05e1a3c7,
    name: "user-password-token-verify"
  }, {
    path: "/user/reset-password",
    component: _a238acfe,
    name: "user-reset-password"
  }, {
    path: "/products/details/:name?",
    component: _7cd4be84,
    name: "products-details-name"
  }, {
    path: "/profile/order/:id?",
    component: _2f709ff7,
    name: "profile-order-id"
  }, {
    path: "/products/:name?",
    component: _fdcca56a,
    name: "products-name"
  }, {
    path: "/",
    component: _f592d694,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
