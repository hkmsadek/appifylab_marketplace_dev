'use strict'

const Product = use("App/Models/Product")
const ProductCategory = use("App/Models/ProductCategory")
const ProductImage = use("App/Models/ProductImage")
const ProductReview = use("App/Models/ProductReview")

const Database = use('Database')
class ProductController {

    async getAllCategories({ request, response }) {
        return response.status(200).json({
            productCategory: await ProductCategory.all()
        })
    }

    // product details by id
    async getProductDetailsById({ request, response, params }) {


        await Product.query()
            .where('slug', params.name)
            .increment('views', 1)

        return response.status(200).json({
            productDetails: await Product.query()
                .where('slug', params.name)
                .with('singleImage')
                .with('avgReview')
                .with('category')
                .with('license')
                .with('version')
                .with('extraInfo')
                .with('singleLicense')
                .with('reviewDetails')
                .with('reviewDetails.user')
                .with('productMeta')
                .first()
        })
    }
    async getProductDetailsByIdsearch({ request, response, params }) {

        return response.status(200).json({
            productDetails: await Product.query()
                .where('title', 'like', "%" + params.name + "%")
                .with('singleImage')
                .fetch()
        })
    }


    // review
    async getAllProducts({ request, response, auth }) {
        return response.status(200).json({
            productDetails: await Product.query()
                .with('singleImage')
                .with('category')
                .with('reviewDetails.user')
                .first()
        })

    }
    async submitReview({ request, response, auth }) {

        let user = await auth.getUser()

        let data = request.all()

        if (data.user_id != user.id) {
            return response.status(401).json({
                message: "You are not authorized to continue further processing!"
            })
        }

        let review = await ProductReview.create({
            user_id: user.id,
            product_id: data.product_id,
            rating: data.rating,
            review: data.review
        })
        
        return response.status(200).json({
            review: review
        })
    }
    async productDetailsSearch({ response, auth, params }) {

        return response.status(200).json({
            productDetails: await Product.query().where('title', 'like', "%" + params.name + "%")
                .fetch()
        })

    }
}

module.exports = ProductController
