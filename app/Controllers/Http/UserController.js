'use strict'

const User = use('App/Models/User')
const Contactus = use('App/Models/Contactus')
const suid = require('rand-token').suid;
const Mail = use("Mail")
const { validateAll } = use('Validator')
const Hash = use('Hash')
const Logger = use('Logger')
class UserController {

    // initial data
    async initData({ request, response, auth }) {
        try {
            let user = await auth.getUser()
            return user
        } catch (error) {
            return false
        }
    }
    // sign up
    async userRegister({ request, response, auth }) {

        let data = request.all()
        const rules = {
            email: 'required|email|unique:users,email',
            password: 'required',
        }
        const messages = {
            'email.required': 'Email is required',
            'email.unique': 'There is an account already exists with this email. Try to login.',
        }
        const validation = await validateAll(data, rules, messages)
        if (validation.fails()) {
            return response.status(401).json(validation.messages())
        }
        let token = suid(16)

        let createUser = {
            email: data.email,
            password: data.password,
            country: data.country,
            accountActivationToken: token,
            fullName: data.fullName,
            ip: request.ip()
        }

        let user = await User.create(createUser)

        // send emails
        let verifyEmailData = {
            fullName: data.fullName,
            email: data.email,
            token: token,
            id: createUser.id
        }
       // this.sendActivationEmail(verifyEmailData)

        return user

    }

    // verify email
    async verifyEmail({ request, response, auth }) {

        let input = request.all()

        if (input.user_email && input.token) {
            let check = await User.query()
                .where('email', input.user_email)
                .where('accountActivationToken', input.token)
                .getCount()

            if (check > 0) {
                await User.query()
                    .where('email', input.user_email)
                    .where('accountActivationToken', input.token)
                    .update({
                        accountActivationToken: '',
                        status: 'active'
                    })
                return response.status(200).json({
                    message: 'Account Activated!'
                })
            }

            return response.status(401).json({
                message: 'Invalid Request!'
            })
        }

        return response.status(401).json({
            message: 'Invalid Request!'
        })
    }


    // login
    async userLogin({ request, response, auth }) {

        const data = request.all()
        const rules = {
          email: 'required|email',
          password: 'required',
        }
        const messages = {
            'email.required': 'Email is required',
            'password.unique': 'Password is required',
        }
        const validation = await validateAll(data, rules, messages)
        if (validation.fails()) {
            return response.status(401).json(validation.messages())
        }
        const isVerified = await User.query().where('email', data.email).where('status', 'active').getCount()
        if (isVerified == 0) {
            return response.status(401).json(
                {
                    'message': 'If you have an account with us, please verify your email. Looks like invalid details.'
                }
            )
        }

        try {

            let user = await auth.query(function (query) {
                query.where('status', 'active')
            }).remember(data.remember).attempt(data.email, data.password)
            return user

        } catch (e) {
            return response.status(401).json(
                {
                    'message': 'Invalid email or password. Please try again.'
                }
            )
        }
    }

    // logout
    async userlogout({ request, response, auth }) {

        try {
            await auth.logout()
            return response.redirect('/')
        } catch (e) {
            return false
        }
    }

    // contact us
    async contactus({ request, response, auth }) {

        let data = request.body

        let createUser = {
            email: data.email,
            message: data.message,
            fullName: data.fullName
        }

        let c = await Contactus.create(createUser)

        return c

    }


    // send password reset code
    async sendPasswordResetCode({ request, response }) {
        let email = request.all().email

        if (!email) {
            return response.staus(401).json({
                message: 'Invalid Request!'
            })
        }

        const check = await User.query().where('email', email).first()
        if (!check) return response.status(401).json({
            message: `If you have any account with this email, you shuould get an email already, check your inbox!`
        })

        let code = this.makeCode(check.id)
        let data = {
            passwordToken: code,
        }

        Mail.send('emails.forgotpassword', data, (message) => {
            message
                .to(email)
                .from('no-reply@appifylab.com', 'Appifylab')
                .subject('Reset You Password')
        })
        await User.query().where('email', email).update({ "passwordToken": code })
        return 'Email Sent'

    }

    // verify password reset code
    async verifyPasswordResetCode({ request, response }) {

        let data = request.all()

        if (!data.token) {
            return response.status(401).json({
                message: 'Invalid Request!'
            })
        }

        const id = this.getIdFromCode(data.token)
        let check = await User.query().where('id', id).where('passwordToken', data.token).getCount()

        if (check == 1) {

            return 'Token verified!'
        }

        return response.status(401).json({
            message: 'Invalid Code'
        })
    }

    // reset password
    async resetPassword({ request, response }) {
        let data = request.all()
        if (!data.newPassword) {
            return response.staus(401).json({
                message: 'Invalid Request!'
            })
        }
        let password = await Hash.make(data.newPassword)
        let check = await User.query().where('passwordToken', data.token).update({ password: password, passwordToken: null })
        return check
    }


    // ********************************* static functions ****************************************
    makeCode(id) {
        let codeId = id * 27
        let code = Math.floor(1000 + Math.random() * 9000)
        return code + '' + codeId
    }

    getIdFromCode(code) {
        let codeId
        if (code) {
            codeId = code.slice(4)
        }
        let mid = codeId / 27
        return mid
    }


    // send email
    async sendActivationEmail(data) {

        try {
            Mail.send('emails.emailVerfication', data, (message) => {
              message
                  .to(data.email)
                  .from('no-reply@appifylab.com', 'Appifylab')
                  .subject('Appifylab account activation')
          })
          console.log('email sent')
          return
        } catch (error) {

          console.log('error while sending email')

        }

    }

}

module.exports = UserController
