'use strict'

const { CodeGuruReviewer } = require("aws-sdk")
const { reset } = require("nodemon")

const Product = use("App/Models/Product")
const ProductLicense = use("App/Models/ProductLicense")
const ProductDownload = use("App/Models/ProductDownload")
const Cart = use("App/Models/Cart")
const Order = use("App/Models/Order")
const OrderDetail = use("App/Models/OrderDetail")
const ProductCoupon = use("App/Models/ProductCoupon")
const User = use("App/Models/User")
const Mail = use('Mail')
class CheckoutController {

    async confirmCheckout({ request, response, auth }) {

        let inputData = request.body
        let input = inputData.ob

        if (!inputData) {
            return response.status(403).json({
                message: 'Invalid request!'
            })
        }


        let user = {}

        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }

        let licenseIds = []
        for (let i of input) {
            licenseIds.push(i.license_id)
        }

        console.log(licenseIds)

        let totalPrice = await this.calculateTotalPrice(licenseIds)
        totalPrice = totalPrice - inputData.discount;

        let order = await Order.create({
            user_id: user.id,
            totalPrice: totalPrice,
            discount: inputData.discount,
            status: 'Pending'
        })



        if (totalPrice == 0) {
            await this.freeDownloadProduct(licenseIds, user, order)
            return order
        }

        let promisArray = []
        if (order) {
            for (let i of input) {
                promisArray.push(this.createOrderDetails({
                    order_id: order.id,
                    product_id: i.product_id,
                    product_version_id: 1,
                    user_id: user.id,
                    price: i.license.price,
                    license_id: i.license.id
                }))
            }
        }

        await Promise.all(promisArray)

        return order
    }

    async  checkCoupon({request,response}){
        let data = request.all();
        var today = new Date();
        let dd = String(today.getDate()).padStart(2,'0');
        let mm = String(today.getMonth()+1).padStart(2,'0');
        let yyyy = today.getFullYear();
        today = yyyy + '-' + mm + '-' + dd
        let product = await ProductCoupon.query().where('code',data.code).first();
        if(product == null){
            return response.status(401).json({
                'message': 'Invalid Coupon Code!',
                'success': true,
            })
        }
        product = await ProductCoupon.query().where('code',data.code).where('validity' , '>=' , today).first();
        if(product == null){
            return response.status(401).json({
                'message': 'Coupon Code expired!',
                'success': true,
            })
        }

       
        
        return response.status(202).json({
            Coupon: product,
            'success': true,
        })
        
        
    }

    async confirmBuyNow({ request, response, auth }) {

        let input = request.body

        let user = {}

        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }
        let license = await ProductLicense.query()
            .where('id', input.licenseId)
            .first()
        license = license.toJSON()
        let array = []
        array.push(license.id)
        console.log(array)
        let totalPrice = await this.calculateTotalPrice(array)

        let order = await Order.create({
            user_id: user.id,
            totalPrice: totalPrice,
            status: 'Pending'
        })



        if (totalPrice == 0) {
            await this.freeDownloadProduct(array, user, order)
            return order
        }
        if (order) {
            await this.createOrderDetails({
                order_id: order.id,
                product_id: input.product_id,
                product_version_id: 1,
                user_id: user.id,
                price: license.price,
                license_id: input.licenseId
            })
        }
        return order
    }


    async freeDownloadProduct(licenseIds, user, order) {
        let products = await ProductLicense.query()
            .whereIn('id', licenseIds)
            .with('products')
            .fetch()

        if (!products) {
            return
        }

        products = products.toJSON()

        let array = []
        let ids = []
        for (let i of products) {
            array.push(this.createDownloadData({
                product_id: i.product_id,
                user_id: user.id,
                license_id: i.id,
                discount: i.products.discount
            }),
                Product.query().where('id', i.product_id).increment('totalPurchased', 1)
            )

            ids.push(i.product_id)
        }

        array.push(this.clearCart(user.id))
        array.push(this.changeOrderStatus(order.id))

        await Promise.all(array)

        if (ids.length > 0) {
            let productsDetails = await OrderDetail.query()
                .where('order_id', order.id)
                .with('products')
                .fetch()

            if (productsDetails) {
                productsDetails = productsDetails.toJSON()
            }

            let emailData = {
                order: order,
                userDetails: user,
                allProducts: productsDetails
            }
            this.sendEmailVerificationLink(emailData)
        }
    }

    createOrderDetails(data) {
        return OrderDetail.create(data)
    }

    createDownloadData(data) {
        return ProductDownload.create(data)
    }

    clearCart(id) {
        return Cart.query()
            .where('user_id', id)
            .delete()
    }

    changeOrderStatus(id) {
        return Order.query()
            .where('id', id)
            .update({
                status: 'Completed'
            })
    }


    // send invoice into the email
    sendEmailVerificationLink(data) {
        return Mail.send('emails.invoice', data, (message) => {
            message
                .to(data.userDetails.email)
                .from('no-reply@appifylab.com', 'Appifylab')
                .subject('Invoice')
        })
    }

    async calculateTotalPrice(licenseIds) {

        let products = await ProductLicense.query()
            .whereIn('id', licenseIds)
            .with('products')
            .fetch()

        if (products) {
            products = products.toJSON()

            let totalPrice = 0
            for (let i of products) {
                totalPrice += (i.products.discount > 0) ? i.price - (i.price * i.products.discount / 100) : i.price
            }

            return totalPrice
        }

        return 0
    }


    async verifyConfirmCheckout({ request, response, auth, params }) {

        try {
            let user = await auth.getUser()
            let verify = await Order.query()
                .where('id', params.id)
                .where('user_id', user.id)
                .with('details')
                .first()
            if (verify) {
                return response.status(200).json({
                    success: true,
                    message: 'Verified',
                    order: verify,
                })
            }

            return response.status(401).json({
                success: false,
                message: 'Invalid Request'
            })
        } catch (error) {
            return response.status(401).json({
                success: false,
                message: 'You are not authorized'
            })
        }

    }

}

module.exports = CheckoutController
