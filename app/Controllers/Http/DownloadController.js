'use strict'

const User = use("App/Models/User")
const Product = use("App/Models/Product")
const ProductDownload = use("App/Models/ProductDownload")
const Helpers = use("Helpers")
const axios = require('axios');
class DownloadController {

    async downloadProduct({ request, response, auth }) {
        try {
            let user = await auth.getUser()
            let input = request.all()

            if (!input.user_id || input.user_id != user.id) {
                return response.status(401).json({
                    message: 'You are not authorized!!'
                })
            }

            if (!input.product_id) {
                return response.statu(403).json({
                    message: "Invalid request!"
                })
            }

            let check = await ProductDownload.query().where('product_id', input.product_id)
                .where('user_id', user.id).getCount()
            if (check == 0) {
                return response.status(401).json({
                    message: `You haven't purchased this product yet`
                })
            }

            if (input.product_id == 23) {
                // E-trainee
                return response.attachment(Helpers.publicPath('/static-files/projects/Etrainee.zip'),
                    'E-trainee.zip')
            }
            if (input.product_id == 24) {
                // beautylab
                return response.attachment(Helpers.publicPath('/static-files/projects/Beautilab.zip'),
                    'Beautilab.zip')
            }
            if (input.product_id == 25) {
                // food station
                return response.attachment(Helpers.publicPath('/static-files/projects/Foodstation.zip'),
                    'Foodstation.zip')
            }
            if (input.product_id == 26) {
                // Rentify
                return response.attachment(Helpers.publicPath('/static-files/projects/Rentify.zip'),
                    'Rentify.zip')
            }
            if (input.product_id == 27) {
                // Ijobs
                return response.attachment(Helpers.publicPath('/static-files/projects/iJobs.zip'),
                    'iJobs.zip')
            }
            if (input.product_id == 28) {
                // appi news
                return response.attachment(Helpers.publicPath('/static-files/projects/Appinews.zip'),
                    'Appinews.zip')
            }
            if (input.product_id == 29) {
                // shop now
                return response.attachment(Helpers.publicPath('/static-files/projects/Shopnow.zip'),
                    'Shopnow.zip')
            }
            if (input.product_id == 32) {
                // tech dot com
                return response.attachment(Helpers.publicPath('/static-files/projects/Techdotcom.zip'),
                    'Techdotcom.zip')
            }
            if (input.product_id == 33) {
                // flutter chat any time
                return response.attachment(Helpers.publicPath('/static-files/projects/al-mp-flutter-customchatapp.zip'),
                    'Techdotcom.zip')
            }
            if (input.product_id == 34) {
                // flutter ecommerce
                return response.attachment(Helpers.publicPath('/static-files/projects/al-mp-flutter-e-commapp.zip'),
                    'Techdotcom.zip')
            }
            if (input.product_id == 35) {
                // flutter food service
                return response.attachment(Helpers.publicPath('/static-files/projects/al-mp-flutter-foodserviceapp.zip'),
                    'Techdotcom.zip')
            }
            if (input.product_id == 36) {
                // flutter hotel booking
                return response.attachment(Helpers.publicPath('/static-files/projects/al-mp-flutter-hotelbookingapp.zip'),
                    'Techdotcom.zip')
            }
            if (input.product_id == 37) {
                // flutter social media
                return response.attachment(Helpers.publicPath('/static-files/projects/al-mp-flutter-socialapp.zip'),
                    'Techdotcom.zip')
            }
            if (input.product_id == 38) {
                // flutter marketplace
                return response.attachment(Helpers.publicPath('/static-files/projects/al-mp-flutter-listifypro.zip'),
                    'Techdotcom.zip')
            }
            if (input.product_id == 39) {
                // flutter marketplace
                return response.attachment(Helpers.publicPath('/static-files/projects/Daslab.zip'),
                    'Daslab.zip')
            }

        } catch (error) {
            console.log(error)
            return response.status(401).json({
                message: 'You are not authorized!'
            })

        }
    }


    async downloadLicenses({ request, response, auth }) {
        try {
            let user = await auth.getUser()
            let input = request.all()

            if (input.user_id != user.id) {
                return response.status(401).json({
                    message: 'You are not authorized!!'
                })
            }
            let check = await ProductDownload.query().where('product_id', input.product_id)
                .where('user_id', user.id).getCount()
            if (check == 0) {
                return response.status(401).json({
                    message: `You haven't purchased this product yet`
                })
            }
            let downloadInfo = await ProductDownload.query()
                .where('product_id', input.product_id)
                .with('license')
                .first()

            if (!downloadInfo) {
                return response.statu(500).json({
                    message: 'Something went wrong! Please try again.'
                })
            }
            downloadInfo = downloadInfo.toJSON()
            if (downloadInfo.license && downloadInfo.license.licenseName == 'Single') {
                response.attachment(Helpers.publicPath('/static-files/projects/single-license.pdf'), 'license.pdf')
            }
            if (downloadInfo.license && downloadInfo.license.licenseName == 'Multiple') {
                response.attachment(Helpers.publicPath('/static-files/projects/multiple-license.pdf'), 'license.pdf')
            }
            if (downloadInfo.license && downloadInfo.license.licenseName == 'Extended') {
                response.attachment(Helpers.publicPath('/static-files/projects/extended-license.pdf'), 'license.pdf')
            }
        }
        catch (error) {
            console.log(error)
            return response.status(401).json({
                message: 'You are not authorized!'
            })

        }
    }
}

module.exports = DownloadController
