'use strict'
const ServiceRequest = use('App/Models/ServiceRequest')
class RequestServiceController {

    async requestForAService({ request, response, auth }) {

        let input = request.body
        
        return ServiceRequest.create({
            email: input.email,
            minBudget: input.minBudget,
            maxBudget: input.maxBudget,
            whatsapp: input.whatsapp ? input.whatsapp : '',
            description: input.description
        })

    }
}

module.exports = RequestServiceController
