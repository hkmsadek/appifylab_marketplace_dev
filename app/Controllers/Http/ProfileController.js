'use strict'
const User = use("App/Models/User")
const ProductDownload = use('App/Models/ProductDownload')
const Order = use('App/Models/Order')
const OrderDetail = use('App/Models/OrderDetail')
const Mail = use('Mail')
const suid = require('rand-token').suid;
const Hash = use('Hash')
class ProfileController {

    async getDetails({ request, response, auth }) {
        try {
            let user = await auth.getUser()

            return User.query()
                .where('id', user.id)
                .with('products')
                .with('products.details')
                .with('products.productSingleImage')
                .with('products.license')
                .with('products.isReviewed', (builder => {
                   builder.where('user_id', user.id)
                }))
                .first()
        } catch (error) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }
    }

    // profile update
    async updateProfile({ request, response, auth }) {

        try {
            let user = await auth.getUser()

            let input = request.body

            return User.query()
                .where('id', user.id)
                .update({
                    fullName: input.fullName
                })
        } catch (error) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }
    }


    // send email verification link
    async sendEmailVerificationLink({ request, response, auth }) {
        const user = await auth.getUser()
        let code = this.makeCode(user.id)
        let data = {
            token: code,
            fullName: user.fullName
        }
        Mail.send('emails.emailreset', data, (message) => {
            message
                .to(user.email)
                .from('no-reply@appifylab.com', 'Appifylab')
                .subject('Email Reset Code')
        })
        await User.query().where('id', user.id).update({ emailResetToken: code })
        return 'Email Sent!'
    }


    // verify email reset token
    async verifyEmailToken({ request, response, auth }) {
        let input = request.body
        let user = await auth.getUser()
        let check = await User.query()
            .where('id', user.id)
            .where('emailResetToken', input.code)
            .getCount()
        if (check == 1) {
            return true
        }
        return response.status(401).json({
            message: 'Invalid Code'
        })
    }


    // change email
    async changeEmail({ request, response, auth }) {
        let input = request.body
        let user = await auth.getUser()

        await User.query()
            .where('id', user.id)
            .where('emailResetToken', input.code)
            .update({
                emailResetToken: ''
            })

        return User.query()
            .where('id', user.id)
            .update({
                email: input.newEmail
            })
    }

    async getOrders({ request, response, auth }) {

        try {
            let user = await auth.getUser()
            let page = request.input('page') ? Number(request.input('page')) : 1
            return Order.query()
                .where('user_id', user.id)
                .orderBy('id', 'desc')
                .paginate(page, 20)
        } catch (error) {
            console.log(error)
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }
    }


    async getOrderDataDetailsById({ request, response, auth, params }) {

        try {
            let user = await auth.getUser()

            return OrderDetail.query()
                .where('user_id', user.id)
                .where('order_id', params.id)
                .with('products')
                .with('license')
                .fetch()

        } catch (error) {

        }
    }


    async getAllDownloads({ request, response, auth }) {

        try {
            let user = await auth.getUser()
            return response.status(200).json({
                allDownloadData: await ProductDownload.query()
                    .where('user_id', user.id)
                    .with('details.isReviewed')
                    .with('license')
                    .with('productSingleImage')
                    
                    .with('isReviewed', (builder => {
                      builder.where('user_id', user.id)
                    }))
                    .fetch()
            })
        } catch (error) {

        }
    }















    // ********************************* static functions ****************************************
    makeCode(id) {
        let codeId = id * 27
        let code = Math.floor(1000 + Math.random() * 9000)
        return code + '' + codeId

    }
    getIdFromCode(code) {
        let codeId
        if (code) {
            codeId = code.slice(4)
        }
        let mid = codeId / 27
        return mid
    }


}

module.exports = ProfileController
