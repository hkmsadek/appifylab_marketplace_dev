'use strict'

const Helpers = use('Helpers')
const Env = use('Env')
var randtoken = require('rand-token');

class UploadController {

    async uploadZip({ request, response }) {
        const avatar = request.file('avatar', {
            types: ['zip'],
            size: '20mb'
        })
        if (avatar) {
            console.log(avatar.toJSON())
            const type = avatar.toJSON().subtype;
            var token = randtoken.generate(50);
            const name = `${token}_${avatar.toJSON().clientName}.${type}`
            // UPLOAD THE IMAGE TO UPLOAD FOLDER 
            await avatar.move(Helpers.publicPath('static-files/projects'), {
                name: name
            })

            if (!avatar.moved()) {
                console.log('error')
                return avatar.error()
            }

            let siteUrl = Env.get('SITE_URL')
            return response.status(200).json({
                message: 'File has been uploaded successfully!',
                file: `${siteUrl}/static-files/projects/${name}`
            })
        }

        return response.status(200).json({
            message: 'Invalid Request!',
            file: ``
        })
    }
}

module.exports = UploadController
