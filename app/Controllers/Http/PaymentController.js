'use strict'
const User = use('App/Models/User')
const Product = use('App/Models/Product')
const ProductLicense = use('App/Models/ProductLicense')
const ProductDownload = use('App/Models/ProductDownload')
const Order = use('App/Models/Order')
const OrderDetail = use('App/Models/OrderDetail')
const Cart = use('App/Models/Cart')
const stripe = require('stripe')('sk_live_Iq2MIXRdmqygaWlOH5sWk0ze00NwNVl6JF');
// const stripe = require('stripe')('sk_test_5oZuBBmWkllC5Dcejt1znKep00V8I9pCXC');
const Mail = use('Mail')

class PaymentController {

    async verifyTokenAndConfirmPayment({ request, response, auth }) {

        let input = request.body

        try {
            let ob = await this.calculateTotalPrice(input.ids);
            let order = await Order.query().where('id',input.order_id).first();
            let user = await auth.getUser();
            let products = ob.products;
            let totalPrice = order.totalPrice;

            let stripe_charge = await
            stripe.charges.create({
                amount: totalPrice * 100,
                currency: 'usd',
                source: input.token,
                // description: `User_id: ${user.id} \n User Name: ${user.fullName} \n Total Charge: ${toatlPrice}`
                description: `Total Charge: ${totalPrice}`
            });
            if (stripe_charge) {
                input.stripeChargeId = stripe_charge.id;

                let array = []
                let ids = []
                for (let i of products) {
                    array.push(this.createDownloadData({
                        product_id: i.product_id,
                        user_id: user.id,
                        license_id: i.id,
                        discount: i.products.discount
                    }),
                        Product.query().where('id', i.product_id).increment('totalPurchased', 1))

                    ids.push(i.product_id)
                }

                array.push(this.clearCart(user.id))
                array.push(this.changeOrderStatus(input.order_id))


                await Promise.all(array)

                if (ids.length > 0) {

                    let order = await Order.query().where('id', input.order_id).first()

                    let productsDetails = await OrderDetail.query()
                        .where('order_id', order.id)
                        .with('products')
                        .fetch()

                    if (productsDetails) {
                        productsDetails = productsDetails.toJSON()
                    }

                    let emailData = {
                        order: order,
                        userDetails: user,
                        allProducts: productsDetails
                    }
                    this.sendEmailVerificationLink(emailData)
                }


                return stripe_charge;
            } else {
                return stripe_charge;
            }

        } catch (error) {
            console.log(error)
            return response.status(204).json({
                message: error
            })
        }

    }

    createDownloadData(data) {
        return ProductDownload.create(data)
    }

    clearCart(id) {
        return Cart.query()
            .where('user_id', id)
            .delete()
    }

    changeOrderStatus(id) {
        return Order.query()
            .where('id', id)
            .update({
                status: 'Completed'
            })
    }


    // calcaulate total price from product id
    async calculateTotalPrice(productIds) {

        let p = await ProductLicense.query()
            .whereIn('id', productIds)
            .with('products')
            .fetch()

        if (p) {
            p = p.toJSON()
            let totalPrice = 0
            for (let i of p) {
                totalPrice += (i.products.discount > 0) ? i.price - (i.price * i.products.discount / 100) : i.price
            }

            return {
                totalPrice: totalPrice,
                products: p
            }
        }

        return 0
    }


    // send invoice into the email
    sendEmailVerificationLink(data) {
        return Mail.send('emails.invoice', data, (message) => {
            message
                .to(data.userDetails.email)
                .from('no-reply@appifylab.com', 'Appifylab')
                .subject('Invoice')
        })
    }
}

module.exports = PaymentController
