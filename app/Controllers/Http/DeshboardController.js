'use strict'

const Product = use("App/Models/Product")
const ProductCategory = use("App/Models/ProductCategory")
const ProductImage = use("App/Models/ProductImage")
const ProductReview = use("App/Models/ProductReview")

class DeshboardController {

    async getAllProductDetails({ request, response }) {

        let [latestThemes] = await Promise.all([
            Product.query()
                .with('singleImage')
                .with('category')
                .with('singleLicense')
                .with('avgReview')
                .orderBy('views', 'desc')
                .orderBy('id', 'desc')
                .limit(6)
                .fetch()
            // Product.query()
            //     .where('type', 'mobile_apps')
            //     .orderBy('totalPurchased', 'desc')
            //     .with('singleImage')
            //     .with('singleLicense')
            //     .with('avgReview')
            //     .with('category')
            //     .limit(2)
            //     .fetch()
        ])


        return response.status(200).json({
            latestThemes: latestThemes
            // mobileApps: mobileApps
        })
    }
}

module.exports = DeshboardController
