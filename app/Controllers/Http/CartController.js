'use strict'

const Cart = use('App/Models/Cart')
const Product = use("App/Models/Product")
const ProductCategory = use("App/Models/ProductCategory")
const ProductImage = use("App/Models/ProductImage")
const ProductReview = use("App/Models/ProductReview")

class CartController {

    async getDetails({ request, response, auth }) {

        try {
            var user = await auth.getUser()
        } catch (error) {
            return response.status(200).json({
                totalItems: [],
                cartDetails: []
            })
        }
        let [totalItems, cartDetails] = await Promise.all([
            Cart.query()
                .where('user_id', user.id)
                .getCount(),
            Cart.query()
                .where('user_id', user.id)
                .with('product')
                .with('product.category')
                .with('product.singleImage')
                .with('license')
                .fetch()
        ])

        return response.status(200).json({
            totalItems: totalItems,
            cartDetails: cartDetails
        })

    }


    // add to cart
    async addToCart({ request, response, auth }) {

        let data = request.all()
        let user = await auth.getUser()

        if (data.user_id != user.id) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }

        let cart = await Cart.create({
            product_id: data.product_id,
            user_id: user.id,
            license_id: data.licenseId
        })

        return response.status(200).json({
            cart: cart
        })
    }

    // remove from cart
    async removeFromCart({ request, response, auth }) {
        let data = request.all()
        let user = await auth.getUser()

        if (data.user_id != user.id) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }

        return Cart.query()
            .where('id', data.id)
            .where('user_id', user.id)
            .delete()
    }
}

module.exports = CartController
