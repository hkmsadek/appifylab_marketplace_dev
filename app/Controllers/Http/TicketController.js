'use strict'

const TiketCategory = use("App/Models/TiketCategory")
const Tiket = use("App/Models/Tiket")

const Database = use('Database')
class TicketController {
    async getAllCategories({ request, response }) {
        return response.status(200).json({
            TiketCategory: await TiketCategory.all()
        })
    }
    async getAllTickets({ request, response }) {
        return response.status(200).json({
            Tiket: await Tiket.all()
        })
    }
    async createTicket({ request, response }) {
        let data = request.all()
        let text = JSON.stringify(data.data)
        let ob = {
            'title': data.title,
            'question': data.question,
            'categoryId': data.categoryId,
            'productId': data.productId,
            'userId': data.userId,
            'otherText': data.otherText,
            'data': text,
        }

        return response.status(200).json({
            Tiket: await Tiket.create(ob)
        })
    }
    // getReplay
    async getReplay({ request, response, params }) {
        let alldata = await Tiket.query()
            .where('id', params.id).
            fetch();


        return alldata

    }
    async updateReplay() {
        // JSON.parse(images),
        let data = request.all()
        let id = data.id
        // let alldata = data.alldata.toJSON()
        let alldata = JSON.stringify(data.alldata)
        let ob = {
            id: 'id',
            data: alldata
        }

    }




}

module.exports = TicketController
