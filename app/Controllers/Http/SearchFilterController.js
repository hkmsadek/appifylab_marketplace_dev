'use strict'

const Product = use("App/Models/Product")
const ProductCategory = use("App/Models/ProductCategory")
const ProductImage = use("App/Models/ProductImage")
const ProductReview = use("App/Models/ProductReview")

const Database = use('Database')

class SearchFilterController {

    // get all products
    async getAllProducts({ request, response }) {
        // let filter = request.input('filter') ? request.input('filter') : 'themes'
        // return response.status(200).json({
        //     allProducts: await Product.query()
        //         .where('type', filter)
        //         .with('singleImage')
        //         .with('category')
        //         .with('avgReview')
        //         .with('singleLicense')
        //         .orderBy('views', 'desc')
        //         .fetch()
        // })

        let filter = request.input('filter');
        let data = Product.query().with('singleImage').with('category').with('avgReview').with('singleLicense')
        if(filter && filter!='All'){
            data.where('type', filter)
        }
        return response.status(200).json({
            allProducts: await data
                .orderBy('views', 'desc')
                .orderBy('id', 'desc')
                .fetch()
        })
    }

    // filter product
    async fillterProduct({ request, response }) {

        let category = request.input('category') ? request.input('category') : ''
        let minRating = request.input('minRating') ? Number(request.input('minRating')) : 0

        let productData = Product.query()
            .where('type', params.type)
            .where('product_category_id', category)
            .with('singleImage')
            .with('category')
            .with('avgReview')
            .orderBy('views', 'desc')
            .orderBy('id', 'desc')


        if (minRating) {
            productData.where('avgRating', '>=', minRating)
        }

        return response.status(200).json({
            allProducts: await productData.paginate(page, 20)
        })

    }
}

module.exports = SearchFilterController
