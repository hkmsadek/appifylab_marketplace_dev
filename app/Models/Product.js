'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const Database = use('Database')

class Product extends Model {

    static boot() {
        super.boot()

        this.addTrait('@provider:Lucid/Slugify', {
            fields: {
                slug: 'title'
            },
            strategy: 'dbIncrement'
        })
    }

    singleImage() {
        return this.hasOne('App/Models/ProductImage')
    }

    category() {
        return this.belongsTo("App/Models/ProductCategory", 'product_category_id')
    }

    avgReview() {
        return this.hasOne('App/Models/ProductReview', 'id', 'product_id').
            select('product_id', Database.raw('cast(AVG(rating) as decimal(10,2)) AS averageRating'))
            .where('status', 'Published')
            .groupBy('product_id')
    }

    reviewDetails() {
        return this.hasMany('App/Models/ProductReview', 'id', 'product_id').where('status', 'Published').orderBy('id', 'desc')
    }
    isReviewed() {
        return this.hasOne('App/Models/ProductReview', 'id', 'product_id').select('id','product_id')
    }

    productMeta() {
        return this.hasMany('App/Models/ProductMeta')
    }

    license() {
        return this.hasMany('App/Models/ProductLicense')
    }

    // singleLicense() {
    //     return this.hasOne('App/Models/ProductLicense')
    // }

    singleLicense() {
        return this.hasOne('App/Models/ProductLicense').orderBy('price', 'desc')
    }

    version() {
        return this.hasOne('App/Models/ProductVersion')
    }

    extraInfo() {
        return this.hasMany('App/Models/ProductExtraFeature')
    }
}

module.exports = Product
