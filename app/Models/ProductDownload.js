'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductDownload extends Model {

    details() {
        return this.belongsTo('App/Models/Product')
    }

    productSingleImage() {
        return this.belongsTo('App/Models/ProductImage', 'product_id', 'product_id')
    }

    license() {
        return this.belongsTo('App/Models/ProductLicense', 'license_id', 'id')
    }
    isReviewed() {
      return this.hasOne('App/Models/ProductReview', 'product_id', 'product_id')
    }
}

module.exports = ProductDownload
