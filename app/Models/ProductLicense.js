'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductLicense extends Model {

    products() {
        return this.belongsTo('App/Models/Product')
    }
}

module.exports = ProductLicense
