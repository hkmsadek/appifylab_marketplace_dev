'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class OrderDetail extends Model {
    products() {
        return this.belongsTo('App/Models/Product')
    }

    license() {
        return this.belongsTo('App/Models/ProductLicense', 'license_id')
    }
}

module.exports = OrderDetail
