'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductExtraFeature extends Model {
}

module.exports = ProductExtraFeature
