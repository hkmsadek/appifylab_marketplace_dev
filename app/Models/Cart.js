'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Cart extends Model {

    product() {
        return this.belongsTo('App/Models/Product', 'product_id', 'id').select('id', 'title', 'price', 'product_category_id', 'discount')
    }

    license() {
        return this.belongsTo('App/Models/ProductLicense', 'license_id' )
    }
}

module.exports = Cart
