'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContactusesSchema extends Schema {
  up () {
    this.create('contactuses', (table) => {
      table.increments()
      table.string('fullName')
      table.string('email')
      table.string('message')
      table.timestamps()
    })
  }

  down () {
    this.drop('contactuses')
  }
}

module.exports = ContactusesSchema
