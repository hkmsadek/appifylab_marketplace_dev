'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UseremailsSchema extends Schema {
  up () {
    this.create('useremails', (table) => {
      table.increments()
      table.string('email')
      table.timestamps()
    })
  }

  down () {
    this.drop('useremails')
  }
}

module.exports = UseremailsSchema
