'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductLicenceSchema extends Schema {
  up() {
    this.create('product_licences', (table) => {
      table.increments()
      table.integer('product_id')
      table.string('licenseName')
      table.string('Priviledge')
      table.timestamps()
    })
  }

  down() {
    this.drop('product_licences')
  }
}

module.exports = ProductLicenceSchema
