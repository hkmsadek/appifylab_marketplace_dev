'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductCouponSchema extends Schema {
  up () {
    this.create('product_coupons', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_coupons')
  }
}

module.exports = ProductCouponSchema
