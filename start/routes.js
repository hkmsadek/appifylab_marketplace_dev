'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')



require('./Routes/user')
require('./Routes/Product')
require('./Routes/Cart')
require('./Routes/Filter')
require('./Routes/deshboard')
require('./Routes/profile')
require('./Routes/download')
require('./Routes/payment')
require('./Routes/request_service')
require('./Routes/checkout')
require('./Routes/ticket')
require('./Routes/order')



// init data

Route.get('/app/user/initData', 'UserController.initData')

Route.post('/upload/zip', 'UploadController.uploadZip')

Route.any('*', 'NuxtController.render')
