'use strict'
const Route = use('Route')

// get all products by type
Route.get('/app/get/product/:type', 'SearchFilterController.getAllProductsByType')