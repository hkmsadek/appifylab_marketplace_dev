'use strict'
const Route = use('Route')

// get profile details
Route.get('/app/get/profile/getDetails', 'ProfileController.getDetails')


// update profile
Route.post('/app/profile/update', 'ProfileController.updateProfile')


// change email ->

Route.post('/app/profile/verify/sendEmail', 'ProfileController.sendEmailVerificationLink')
Route.post('/app/profile/verify/emailToken', 'ProfileController.verifyEmailToken')

Route.post('/app/profile/change/email', 'ProfileController.changeEmail')

//taking user email
Route.post('/app/home/user/email', 'UseremailController.addUseremail')


// get my downloads
Route.get('/app/profile/get/downloads', 'ProfileController.getAllDownloads')