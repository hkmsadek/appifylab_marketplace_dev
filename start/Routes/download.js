'use strict'

const Route = use('Route')

// get all deshboard info
Route.get('/app/download/product', 'DownloadController.downloadProduct')

Route.get('/app/download/license', 'DownloadController.downloadLicenses')