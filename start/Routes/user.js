'use strict'
const Route = use('Route')


Route.post('/app/user/login', 'UserController.userLogin')
Route.post('/app/user/register', 'UserController.userRegister')
Route.get('/app/user/logout', 'UserController.userlogout')

Route.post('/app/contactus', 'UserController.contactus')



// verify email
Route.post('/app/user/verify/email', 'UserController.verifyEmail')


// ******************** forget password **************************
Route.post('/app/user/send/passwordResetCode', 'UserController.sendPasswordResetCode')
Route.post('/app/user/verify/passwordResetCode', 'UserController.verifyPasswordResetCode')
Route.post('/app/user/password/reset', 'UserController.resetPassword')