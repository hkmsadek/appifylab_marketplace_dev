'use strict'

const Route = use('Route')

// get all product categroy
Route.get('/app/get/tiket/allCategory', 'TicketController.getAllCategories')
Route.get('/app/get/tiket/allTikets', 'TicketController.getAllTickets')
Route.post('/app/get/tiket/createTicket', 'TicketController.createTicket')


// replay
Route.get('/app/get/tiket/replay/:id', 'TicketController.getReplay')
