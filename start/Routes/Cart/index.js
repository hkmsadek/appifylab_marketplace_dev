'use strict'

const Route = use('Route')

// get curt details
Route.get('/app/cart/getDetails', 'CartController.getDetails')


// add to cart
Route.post('/app/cart/addToCart', 'CartController.addToCart')


// delete from the cart
Route.post('/app/cart/delete', 'CartController.removeFromCart')



