'use strict'

const Route = use('Route')


// checkout
Route.post('/app/user/checkout', 'CheckoutController.confirmCheckout')
Route.post('/app/user/checkout/checkCoupon', 'CheckoutController.checkCoupon')
Route.post('/app/user/checkout/buynow', 'CheckoutController.confirmBuyNow')


Route.get('/app/user/verify-for-checkout/:id', 'CheckoutController.verifyConfirmCheckout')