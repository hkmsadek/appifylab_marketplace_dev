'use strict'

const Route = use('Route')

// send stripe token from front-end to backend
Route.post('/app/payment/verify', 'PaymentController.verifyTokenAndConfirmPayment')
