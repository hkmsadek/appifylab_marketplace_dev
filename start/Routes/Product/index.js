'use strict'

const Route = use('Route')

// get all product categroy
Route.get('/app/get/product/allCategory', 'ProductController.getAllCategories')


// get all products by type
// Route.get('/app/get/product/:type', 'SearchFilterController.getAllProductsByType')
Route.get('/app/get/products', 'SearchFilterController.getAllProducts')

// get product details by id
Route.get('/app/get/productDetails/:name', 'ProductController.getProductDetailsById')
Route.get('/app/get/productDetailsSearch/:name', 'ProductController.productDetailsSearch')


// submit review
Route.post('/app/post/submitReview', 'ProductController.submitReview')



// Product filter
Route.get('/app/filter/product', 'SearchFilterController.fillterProduct')