'use strict'
const Route = use('Route')


// person's order
Route.get('/app/profile/get/orders', 'ProfileController.getOrders')

// get order details
Route.get('/app/profile/get/orders/detais/:id', 'ProfileController.getOrderDataDetailsById')
