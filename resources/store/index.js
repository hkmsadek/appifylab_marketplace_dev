export const strict = false

export const state = () => ({
    authUser: false,
    productCategory: [],
    cartItem: [],
    cartItemNumber: 0,

    addToCartModal: false,
    singleCart: false
})

// common getters
export const getters = {
    getAuthUser(state) {
        return state.authUser
    },

    getProductCategory(state) {
        return state.productCategory
    },

    getCartItem(state) {
        return state.cartItem
    },

    getCartItemNumber(state) {
        return state.cartItemNumber
    },

    getAddToCartModal(state) {
        return state.addToCartModal
    },

    getSingleCart(state) {
        return state.singleCart
    }
}

// mutations for changing data from action
export const mutations = {

    updateAuthUser(state, user) {
        state.authUser = user
    },

    setProductCategory(state, data) {
        state.productCategory = data
    },

    setCartItem(state, data) {
        state.cartItem = data
    },

    setCartItemNumber(state, data) {
        state.cartItemNumber = data
    },

    setAddToCartModal(state, data) {
        state.addToCartModal = data
    },

    setSingleCart(state, data) {
        state.singleCart = data
    }

}

// actionns for commiting mutations
export const actions = {
    async nuxtServerInit({ commit, state }, { $axios }) {
        try {
            let { data } = await $axios.get('/app/user/initData')
            // state.authUser = data
            commit('updateAuthUser', data)
        } catch (e) {
        }
        console.log('sss')
    },

    updateAuthUser(state, user) {
        state.authUser = user
    },

}
