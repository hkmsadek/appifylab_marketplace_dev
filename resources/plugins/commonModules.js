import Vue from 'vue'
import { mapGetters } from 'vuex'

Vue.mixin({

  methods: {

  },

  computed: {
    ...mapGetters({
      authUser: "getAuthUser",
      productCategory: 'getProductCategory',
      cartNumber: 'getCartItemNumber',
      cartDetails: 'getCartItem'
    })
  },
})
