//MENU
$(document).ready(function(){
  // var scrollTop = 0;
  $(document).scroll(function(){
    var scrollTop = $(this).scrollTop();
    if (scrollTop >= 20) {
      $('#_1menu').addClass( "menu_fixed" );
    } else  {
      $('#_1menu').removeClass("menu_fixed");
    } 
  });

  const dropdown = $("._1menu_profile");

  $("._1menu_profile button").on('click', () => {
    dropdown.toggleClass("_1menu_profile_active");
  });

  
  $(document).mouseup(e => {
      if (!dropdown.is(e.target) && dropdown.has(e.target).length === 0){
        dropdown.removeClass('_1menu_profile_active');
      }
  });

  $(document).mouseup(function(e) 
  {
      var container = $("._1menu_profile button");

      // if the target of the click isn't the container nor a descendant of the container
      if (container.has(e.target).length == 0) 
      {
        $("._1menu_profile_dropdown").removeClass("_dropdown_active");
      }
  });

  

  // $("._1menu_profile_dropdown").click(function(e) {
  //   e.stopPropagation();
  // });

  // if($("._1menu_profile_dropdown").hasClass("_dropdown_active").length > 0) {
  //   $(document).click(function() {
  //     $("._1menu_profile_dropdown").removeClass("_dropdown_active");
  //   });
  // }

  

  // mobile menu

  $("._menu_bar span").click(function() {
    $("._mobile_menu_overlay").addClass("_mobile_menu_active");
    $("._mobile_menu_list").addClass("_side_left");
  });

  $("._mobile_menu_close span").click(function() {
    $("._mobile_menu_overlay").removeClass("_mobile_menu_active");
    $("._mobile_menu_list").removeClass("_side_left");
  });


});