'use strict'

const resolve = require('path').resolve

module.exports = {
  env: {
    // baseUrl: process.env.BASE_URL,
    Stripe_Publishable_key: process.env.Stripe_Publishable_key,
    Stripe_Secret_key: process.env.STRIPE_SECRET_kEY
  },

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/moment',
    '@nuxtjs/google-adsense'
  ],

  axios: {
    baseURL: 'https://free.appifylab.com/'
  },
  buildModules: [
    '@nuxtjs/google-analytics'
  ],
  googleAnalytics: {
    id: 'UA-67475707-4'
  },
  'google-adsense': {
    id: 'ca-pub-4511789265076314'
  },
  plugins: [
    '~plugins/axios',
    '~plugins/iview',
    '~plugins/vuesax',
    '~plugins/commonModules',
    '~plugins/vue-lazy-load',

  ],


  /*
   ** Headers of the page
   */
  head: {
    title: 'Appifylab MarketPlace',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        property: 'fb:pages',
        content: '310060563192757'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Appifylab MarketPlace'
      }
    ],
    link: [{
        rel: "icon",
        type: "image/x-icon",
        href: '/static/Appifylogo.png'
        // href: "/logo.png"
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Lora:400,400i,700,700i&display=swap'
      },
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.8.2/css/all.css',
        integrity: 'sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay',
        crossorigin: 'anonymous'
      },
      {
        rel: 'stylesheet',
        href: "/css/bootstrap.css"
      },
      {
        rel: 'stylesheet',
        href: "/css/common.css"
      },
      {
        rel: 'stylesheet',
        href: "/css/main.css"
      },
    ],
    script: [

      // {
      //   'src': 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js',
      //   'body': false,
      //   "data-ad-client" : "ca-pub-4511789265076314",
      //   'async': true,
      // },
      {
        src: 'https://code.jquery.com/jquery-3.4.0.min.js',
        integrity: 'sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=',
        crossorigin: "anonymous"
      },
      {
        src: '/js/script.js',
        body: true
      },
      {
        src: "https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit",
        async: true,
        defer: true
      },
      {
        src: 'https://js.stripe.com/v3/',
        body: true
      },
    ]
  },
  /*
   ** Global CSS
   */
  css: [{
    src: '~assets/css/main.css',
    lang: "sass"
  }],
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#3177f1'
  },
  /*
   ** Point to resources
   */
  srcDir: resolve(__dirname, '..', 'resources')
}
